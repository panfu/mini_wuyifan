'use strict';
var domain = require('domain');
var express = require('express');
var request = require('request-json');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var todos = require('./routes/todos');
var cloud = require('./cloud');
var sign = require('./sign.js');
var AV = require('leanengine');
var WechatToken = AV.Object.extend('WechatToken')

var app = express();

// 设置 view 引擎
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.static('public'));

// 加载云代码方法
app.use(cloud);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// 未处理异常捕获 middleware
app.use(function(req, res, next) {
  var d = null;
  if (process.domain) {
    d = process.domain;
  } else {
    d = domain.create();
  }
  d.add(req);
  d.add(res);
  d.on('error', function(err) {
    console.error('uncaughtException url=%s, msg=%s', req.url, err.stack || err.message || err);
    if(!res.finished) {
      res.statusCode = 500;
      res.setHeader('content-type', 'application/json; charset=UTF-8');
      res.end('uncaughtException');
    }
  });
  d.run(next);
});

app.get('/sign', function(req, res) {

  var param = req.query.param
  console.log(param)
  // 这里需要加缓存判断

  var query = new AV.Query(WechatToken);
  var is_e  = false;
  var client = request.createClient('https://api.weixin.qq.com');
  client.headers['Content-Type'] = 'application/json';
  query.get('5641ce5100b0023c3abf8d50').then(function (t) {
    if (t && t.get('e_at') > Date.now()) {
      console.log('尚未过期', t.get('value'));
      client.get('https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=' + t.get('value') + '&type=jsapi', function(tcerr, tcres, tbody) {
        var signature = sign(tbody.ticket, 'http://oping.avosapps.com/' + param  )
        res.json(signature);
      });
    } else {
      
      client.get('/cgi-bin/token?grant_type=client_credential&appid=wx37a4c48d2ea59c29&secret=318e92d8a73efd6eefdda264276cae88', function(cerr, cres, body) {
        // {"access_token":"ACCESS_TOKEN","expires_in":7200}
        console.log('已经过期，需要生成新的：', body.access_token, body.expires_in);

        // body.expires_in * 1000
        t.set('value', body.access_token);
        t.set('e_at', Date.now() + (body.expires_in * 1000));
        t.save();

        client.get('https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=' + body.access_token + '&type=jsapi', function(tcerr, tcres, tbody) {
          var signature = sign(tbody.ticket, 'http://oping.avosapps.com/' + param  )
          res.json(signature);
        });
      });
    }


  });

});

app.get('/', function(req, res) {
  res.render('index', { currentTime: new Date() });
});

// 可以将一类的路由单独保存在一个文件中
app.use('/todos', todos);

// 如果任何路由都没匹配到，则认为 404
// 生成一个异常让后面的 err handler 捕获
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// 如果是开发环境，则将异常堆栈输出到页面，方便开发调试
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) { // jshint ignore:line
    var statusCode = err.status || 500;
    if(statusCode === 500) {
      console.error(err.stack || err);
    }
    res.status(statusCode);
    res.render('error', {
      message: err.message || err,
      error: err
    });
  });
}

// 如果是非开发环境，则页面只输出简单的错误信息
app.use(function(err, req, res, next) { // jshint ignore:line
  res.status(err.status || 500);
  res.render('error', {
    message: err.message || err,
    error: {}
  });
});

module.exports = app;
